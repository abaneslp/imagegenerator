// XmlWriter.h: Interface for xml writer
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __XMLWRITER_H
#define __XMLWRITER_H

#include <string>
#include <vector>
#include <stack>


typedef std::stack<std::string> StackSrtrings;


class Annotation
{
private:
	FILE* fp = NULL;
	int level = 0;
	std::string xmlFile = "";
	StackSrtrings tagStack;
	std::vector<std::string> vectAttrData;

public:
	Annotation(std::string path);
	~Annotation();

	void createChild(std::string tag, std::string value);
	void createTag(std::string tag);
	void closeLastTag();
	void cloaseAllTag();
	void addAttributes(std::string attr, std::string value);
	void adddComment(std::string comment);
};


#endif // !__XMLWRITER_H
