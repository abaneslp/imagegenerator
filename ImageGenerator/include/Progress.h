// Progress.h: Interface for progress indicator
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#ifndef __PROGRESS_H
#define __PROGRESS_H

#include <iostream>
#include <string>


class Progress
{
private:
	float __progress{ 0.0f };
	//std::recursive_mutex __mutex;
	size_t barWidth{ 60 };
	std::string __fill{ "#" }, __remainder{ " " }, statusText{ "" };

public:
	Progress();
	~Progress() {}

	void setProgress(float value);
	void setBarWidth(size_t wwidth);
	void setStatusText(const std::string& status);
	void writeProgress(std::ostream& os = std::cout);
	void fillBarProgressWith(const std::string& chars);
	void fillBarRemainderWith(const std::string& chars);
	void update(float value, std::ostream& os = std::cout);
};


#endif // !__PROGRESS_H
