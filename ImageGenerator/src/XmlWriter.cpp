// XmlWriter.cpp: Implementation for xml writer
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#pragma warning(disable : 4996)

#include <stdarg.h>
#include <iostream>

#include "XmlWriter.h"


Annotation::Annotation(std::string path) : xmlFile(path)
{
	fp = fopen(xmlFile.c_str(), "w");

	if (fp == NULL)
	{
		std::cout << "Unable to open output file" << std::endl;
		return;
	}

	else
		fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"\?>");
}


Annotation::~Annotation()
{
	if (fp != NULL)
		fclose(fp);

	vectAttrData.clear();
}


void Annotation::createChild(std::string tag, std::string value)
{
	fprintf(fp, "\n");
	for (int i = 0; i < level; i++)
		fprintf(fp, "\t");

	fprintf(fp, "<%s", tag.c_str());
	while (0 < vectAttrData.size() / 2)
	{
		std::string temp = vectAttrData.back();
		fprintf(fp, " %s=", temp.c_str());
		vectAttrData.pop_back();
		temp = vectAttrData.back();
		fprintf(fp, "\"%s\"", temp.c_str());
		vectAttrData.pop_back();
	}

	vectAttrData.clear();
	fprintf(fp, ">%s</%s>", value.c_str(), tag.c_str());
}


void Annotation::createTag(std::string tag)
{
	fprintf(fp, "\n");

	for (int i = 0; i < level; i++)
		fprintf(fp, "\t");

	fprintf(fp, "<%s", tag.c_str());

	while (0 < vectAttrData.size() / 2)
	{
		std::string sTmp = vectAttrData.back();
		fprintf(fp, " %s=", sTmp.c_str());
		vectAttrData.pop_back();
		sTmp = vectAttrData.back();
		fprintf(fp, "\"%s\"", sTmp.c_str());
		vectAttrData.pop_back();
	}

	vectAttrData.clear();
	fprintf(fp, ">");
	tagStack.push(tag);
	level++;
}


void Annotation::closeLastTag()
{
	fprintf(fp, "\n");
	level--;

	for (int i = 0; i < level; i++)
		fprintf(fp, "\t");

	fprintf(fp, "</%s>", tagStack.top().c_str());
	tagStack.pop();
	return;
}


void Annotation::cloaseAllTag()
{
	while (tagStack.size() != 0)
	{
		fprintf(fp, "\n");
		level--;

		for (int i = 0; i < level; i++)
			fprintf(fp, "\t");

		fprintf(fp, "</%s>", tagStack.top().c_str());
		tagStack.pop();
	}
	return;
}


void Annotation::addAttributes(std::string attr, std::string value)
{
	vectAttrData.push_back(value);
	vectAttrData.push_back(attr);
}


void Annotation::adddComment(std::string comment)
{
	fprintf(fp, "\n");

	for (int i = 0; i < level; i++)
		fprintf(fp, "\t");

	fprintf(fp, "<!--%s-->", comment.c_str());
}
