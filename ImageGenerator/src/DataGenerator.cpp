// DataGenerator.cpp: Defines the entry point for the application.
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <filesystem>
#include <iostream>
#include <chrono>
#include <future>
#include <thread>

#include "Progress.h"
#include "Process.h"
#include "Image.h"
#include "Util.h"

using namespace util;
namespace fs = std::filesystem;

std::string BG_PATH = CWD() + "/data/source/background/";
std::string OL_PATH = CWD() + "/data/source/overlay/";

void invokeSingleProcess(Image&, Image& png, int&, int&, util::Arguments&, std::string&);
void invokeMultipleProcess(Image&, Image&, int&, util::Arguments&, std::string&);


int main()
{
	std::cout << "// DATA GENERATOR - WIN\n//" << std::endl;
	std::cout << "// Date: " << DATETIME("log") << std::endl;
	std::cout << "//\n//////////////////////////////////////////////////////////////////////\n\n";

	std::string destination = DESTINATION();
	Image background(BG_PATH, JPG);
	Image overlay(OL_PATH, PNG);

	int imgCount = 0;
	int pngTracker = 0;

	Clock total;
	Arguments args;
	Progress prog;

	std::chrono::microseconds span(1);

	if (args.mode == 1)
	{
		std::cout << "\nStart Single - Object Images Generation" << std::endl;

		auto proc1 = std::async(invokeSingleProcess, std::ref(background), std::ref(overlay), std::ref(imgCount), std::ref(pngTracker), std::ref(args), std::ref(destination));

		while (proc1.wait_for(span) != std::future_status::ready)
		{
			prog.update((static_cast<double>(pngTracker) / static_cast<double>(overlay.count)) * 100);
			prog.setStatusText("\t" + std::to_string(pngTracker) + "/" + std::to_string(overlay.count) + " PNG Images\t| Total Count: " + std::to_string(imgCount));
		}
	}

	else if (args.mode == 2)
	{
		std::cout << "\nStart Multiple - Object Images Generation" << std::endl;

		auto proc2 = std::async(invokeMultipleProcess, std::ref(background), std::ref(overlay), std::ref(imgCount), std::ref(args), std::ref(destination));

		while (proc2.wait_for(span) != std::future_status::ready)
		{
			prog.update((static_cast<double>(imgCount) / static_cast<double>(args.maxImage)) * 100);
			prog.setStatusText("\t" + std::to_string(imgCount) + "/" + std::to_string(args.maxImage) + " Multi-Object Images Generated");
		}
	}

	else if (args.mode == 3)
	{
		std::cout << "\nStart Single & Multiple - Object Images Generation\n" << std::endl;
		int sngCount = 0;
		int mulCount = 0;

		auto proc31 = std::async(invokeSingleProcess, std::ref(background), std::ref(overlay), std::ref(sngCount), std::ref(pngTracker), std::ref(args), std::ref(destination));
		auto proc32 = std::async(invokeMultipleProcess, std::ref(background), std::ref(overlay), std::ref(mulCount), std::ref(args), std::ref(destination));

		while (proc31.wait_for(span) != std::future_status::ready || proc32.wait_for(span) != std::future_status::ready)
		{
			prog.update((static_cast<double>(pngTracker) / static_cast<double>(overlay.count)) * 100);
			prog.setStatusText("\t" + std::to_string(pngTracker) + "/" + std::to_string(overlay.count) + " PNG Images\t| Multi-Object Images: " + std::to_string(mulCount) + "/" + std::to_string(args.maxImage));
		}
	}

	do
	{
		std::cout << "\nDone! Total time execution: " << total.stop() / 1000000 << " seconds";
		std::cout << "\nPress enter to exit ...";
	} while (std::cin.get() != '\n');

	return 0;
}


void invokeSingleProcess(Image& jpg, Image& png, int& count, int& pngTracker, util::Arguments& args, std::string& destination)
{
	cv::Point location(args.hPstn, args.vPstn);
	size_t pngTotal = png.count;
	size_t div = pngTotal / 10;
	size_t start = 0;
	size_t end = div;
	int p = (args.mode == 3) ? 10 : 0;

	std::future<bool> async1 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start, end, p + 0);
	std::future<bool> async2 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 1);
	std::future<bool> async3 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 2);
	std::future<bool> async4 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 3);
	std::future<bool> async5 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 4);
	std::future<bool> async6 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 5);
	std::future<bool> async7 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 6);
	std::future<bool> async8 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 7);
	std::future<bool> async9 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, end += div, p + 8);
	std::future<bool> async10 = std::async(proc::genSingleObject, std::ref(png), std::ref(jpg), std::ref(count), std::ref(pngTracker), std::ref(location), std::ref(args), destination, start += div, pngTotal, p + 9);
}


void invokeMultipleProcess(Image& jpg, Image& png, int& count, util::Arguments& args, std::string& destination)
{
	args.maxImage = (args.maxImage < 10) ? 10 : args.maxImage;
	int div = args.maxImage / 10;
	int mod = args.maxImage % div;
	int end = div + mod;
	int p = (args.mode == 3) ? 20 : 0;

	std::future<bool> async1 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 0);
	std::future<bool> async2 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 1);
	std::future<bool> async3 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 2);
	std::future<bool> async4 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 3);
	std::future<bool> async5 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 4);
	std::future<bool> async6 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 5);
	std::future<bool> async7 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 6);
	std::future<bool> async8 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 7);
	std::future<bool> async9 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(div), std::ref(args), destination, p + 8);
	std::future<bool> async10 = std::async(proc::genMultipleObjects, std::ref(jpg), std::ref(png), std::ref(count), std::ref(end), std::ref(args), destination, p + 9);
}
