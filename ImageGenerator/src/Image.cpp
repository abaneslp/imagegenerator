// Image.cpp: Implementation for Image class
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include <filesystem>
#include "Image.h"

#include <iostream>

namespace fs = std::filesystem;


static const char* EXTN[T] = { ".png", ".jpg" };


Image::Image(std::string& __path, ExtnType __type, const int n)
{
	type = EXTN[__type];
	source = __path;
	num = n;

	loadImages();
}


Image::Image(const Image& img)
{
	fn = img.fn;
	count = img.count;
	type = img.type;
	num = img.num;
	source = img.source;
}


void Image::loadImages()
{
	if (!fs::exists(source))
	{
		std::cout << "ERROR: Could not found " << source << std::endl;
		return;
	}

	for (auto& img : fs::recursive_directory_iterator(source))
	{
		if (fs::path(img).extension() == type)
		{
			fs::path imgPath = img.path();
			fn.push_back(imgPath.string());
		}
	}
	count = fn.size();
}


ImageObject::ImageObject(cv::Mat obj, std::string path)
{
	filepath = path;
	width = obj.cols;
	height = obj.rows;
}


void ImageObject::addObject(std::string fname)
{
	std::vector<std::string> objectInfo;
	std::string object = fs::path(fname).filename().string();
	object.erase(object.length() - 4, 4);
	objectList.push_back(object);

	std::string delimiter = "_";
	std::string info = "";
	size_t p = 0;

	while ((p = object.find(delimiter)) != std::string::npos)
	{
		info = object.substr(0, p);
		objectInfo.push_back(info);
		object.erase(0, p + delimiter.length());
	}

	objectCategory.push_back(objectInfo[0]);
	objectName.push_back(objectInfo[1]);
	objectPose.push_back(objectInfo[2]);

	occlusionCate.push_back("None");
	occlusionName.push_back("None");
	occlusionPose.push_back("None");
	occlusionRate.push_back(std::to_string(0));
}


void ImageObject::addImage(cv::Mat img, std::string path)
{
	filepath = path;
	width = img.cols;
	height = img.rows;
}
