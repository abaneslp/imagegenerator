//Util.cpp: Implementation for the helper classes
// 
// Author: L. Abanes | September 2020
//
////////////////////////////////////////////////////////////////////////

#pragma warning(disable : 4996)

#include <filesystem>
#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>

#include "Util.h"

#define now()			std::chrono::high_resolution_clock::now()
#define duration(t)		std::chrono::duration_cast<std::chrono::microseconds>(t).count()

using namespace util;
namespace fs = std::filesystem;

Utility* Utility::util_Instance = 0;


Utility* Utility::utilInstance() throw()
{
	if (util_Instance == 0)
		util_Instance = new Utility();

	return util_Instance;
}


int Utility::getInput()
{
	int __ = 0;
	std::string input = "";

	std::getline(std::cin, input);
	if (input != "\n")
	{
		std::istringstream ss(input);
		ss >> __;
	}

	return __;
}


std::string Utility::getWorkDir()
{
	fs::path cwd = fs::current_path();
	return cwd.string();
}


std::string Utility::getDatetime(std::string t) throw()
{
	time_t currTime;
	tm* currTm;
	char dateTime[100];

	time(&currTime);
	currTm = localtime(&currTime);

	if (t == "file")
		strftime(dateTime, sizeof(dateTime), "%Y%m%d", currTm);
	else if (t == "log")
		strftime(dateTime, sizeof(dateTime), "%Y-%m-%d-%X", currTm);

	return std::string(dateTime);
}


std::string Utility::getFileDestination() throw()
{
	std::string destination;
	std::string filepath = CWD() + "/data/output/";
	fs::create_directories(filepath);

	std::string currDestination = DATETIME("file");
	std::string tempDestination = filepath + currDestination + "-0/";
	int it = 1;

	do
	{
		if (!fs::exists(tempDestination))
		{
			fs::create_directories(tempDestination + "objects/");
			fs::create_directories(tempDestination + "annotations/");
			destination = tempDestination;
		}
		tempDestination = filepath + currDestination + "-" + std::to_string(it) + "/";
		it += 1;
	} while (destination.empty());

	return destination;
}


std::string Utility::getSerialNumber(int& count, int n) throw()
{
	count += 1;
	std::string series = std::to_string(n) + "000000000" + std::to_string(count);

	if (series.length() > 10)
		series.erase(2, series.length() - 10);

	return series;
}


Clock::Clock()
{
	__start = now();
}


double Clock::stop() throw()
{
	return duration(now() - __start);
}


Arguments::Arguments()
{
	std::cout << "Mode (1: one object, 2:multiple object, 3:both) [1]: ";
	mode = INPUT();
	mode = (mode == 0) ? 1 : mode;

	if (mode != 2)
	{
		std::cout << "Horizontal position for one object image [8]: ";
		hPstn = INPUT();
		std::cout << "Vertical position for one object image [6]: ";
		vPstn = INPUT();
		std::cout << "Rotation angle step [10]: ";
		step = INPUT();
		std::cout << "Maximum rotation angle [180]: ";
		maxRot = INPUT();

		step = (step == 0) ? 10 : step;
		hPstn = (hPstn == 0) ? 8 : hPstn;
		vPstn = (vPstn == 0) ? 6 : vPstn;
		maxRot = (maxRot == 0) ? 180 : maxRot;
		maxImage = (maxImage == 0) ? 80000 : maxImage;

	}

	if (mode == 2 || mode == 3)
	{
		std::cout << "Number of multi object images [80000]: ";
		maxImage = INPUT();

		maxImage = (maxImage == 0) ? 80000 : maxImage;
	}
}
