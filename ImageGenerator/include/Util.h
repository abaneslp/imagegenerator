// Util.h: Interface for the helper classes
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __UTIL_H
#define __UTIL_H

#include <string>
#include <chrono>


namespace util
{
#define INPUT()					Utility::utilInstance() -> getInput()
#define DESTINATION()			Utility::utilInstance() -> getFileDestination()
#define SERIAL(x, y)			Utility::utilInstance() -> getSerialNumber(x, y)
#define DATETIME(t)				Utility::utilInstance() -> getDatetime(t)
#define CWD()					Utility::utilInstance() -> getWorkDir()
#define EXE()					Utility::utilInstance() -> getExeDir()

	typedef std::chrono::high_resolution_clock::time_point var_t;


	class Clock
	{
	private:
		var_t __start;

	public:
		Clock();
		~Clock() {}

		double stop() throw();
	};


	struct Arguments
	{
	public:
		int mode = 0;
		int maxImage = 0;
		int hPstn = 0;
		int vPstn = 0;
		int step = 0;
		int maxRot = 0;
		int segmented = 0;
		int serialNum = 0;
		int truncated = 1;
		int difficult = 0;
		int depth = 3;

		std::string database = "REGI201908";
		std::string annotation = "PASCAL REGI201908";
		std::string image = "flickr";
		std::string flickrid = "341012865";
		std::string ownerflickrid = "Fried Camels";
		std::string folder = "REGI2019080";
		std::string ownerName = "jaechul kim";

		Arguments();
		~Arguments() {}
	};


	class Utility
	{
	private:
		static Utility* util_Instance;

	public:
		static Utility* utilInstance() throw();

		std::string getWorkDir();
		std::string getFileDestination() throw();
		std::string getSerialNumber(int& count, int n) throw();
		std::string getDatetime(std::string t) throw();
		int getInput();

		Utility() {}
		~Utility() {}
	};
};


#endif // !__UTIL_H
