// Process.h: Interface for image processing functions
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#ifndef __PROCESS_H
#define __PROCESS_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <vector>
#include <string>

#include "XmlWriter.h"
#include "Image.h"
#include "Util.h"


namespace proc
{
	const float SCALE_MIN = 0.9;
	const float SCALE_MAX = 1.0;

	cv::String getRandomImage(Image& img);
	cv::Mat rotateImage(cv::Mat original, int angle);
	cv::Mat pasteImage(cv::Mat jpg, cv::Mat png, cv::Point2i position);
	bool genSingleObject(Image& png, Image& jpg, int& count, int& tracker, cv::Point2i& location, util::Arguments& args,
		std::string destination, size_t start = 0, size_t end = 0, int n = 1);
	bool genMultipleObjects(Image& jpg, Image& png, int& count, int& max, util::Arguments& args, std::string destination, int p);
	void addAnnotation(Annotation& xml, ImageObject& img, util::Arguments& args);
}


#endif // !__PROCESS_H
