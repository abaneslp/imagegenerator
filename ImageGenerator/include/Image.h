// Image.h: Interface for Image class
// 
// Author: L. Abanes | August 2020
//
//////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __IMAGE_H
#define __IMAGE_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <vector>


typedef enum IMG_TYPE
{
	PNG = 0,
	JPG = 1,
	T = 2,
}ExtnType;


class Image
{
private:
	std::string source;
	const char* type;
	int num;

public:
	std::vector<cv::String> fn;
	size_t count;

private:
	void loadImages();

public:
	Image(std::string& __path, ExtnType __type, const int = 0);
	Image(const Image& img);
	~Image() {}
};


class ImageObject
{
public:
	int width = 0;
	int height = 0;

	std::string filepath = "";
	std::vector<int> hmin;
	std::vector<int> wmin;
	std::vector<int> hmax;
	std::vector<int> wmax;

	std::vector<std::string> objectList;
	std::vector<std::string> objectName;
	std::vector<std::string> objectCategory;
	std::vector<std::string> objectPose;
	std::vector<std::string> occlusionCate;
	std::vector<std::string> occlusionName;
	std::vector<std::string> occlusionPose;
	std::vector<std::string> occlusionRate;
	std::vector<double> visibleRate;

public:
	ImageObject(cv::Mat obj, std::string path);
	ImageObject() {}
	~ImageObject() {}

	void addObject(std::string fname);
	void addImage(cv::Mat img, std::string path);
};

#endif // !__IMAGE_H
